package com.example.postegreapi.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.postegreapi.entity.Book;
import com.example.postegreapi.repository.BookRepository;


@RestController
@RequestMapping("/books")
public class BookController {

    private BookRepository bookRepository;

    public BookController(BookRepository bookRepositoryInjected) {
        this.bookRepository = bookRepositoryInjected;
    }

    @GetMapping("")
    public List<Book> getAllBooks() {
        return this.bookRepository.findAll();
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<Book> getBookById(@PathVariable Long bookId) {
        var optionalBook = this.bookRepository.findById(bookId);
        if (optionalBook.isPresent()) {
            var book = optionalBook.get();
            return ResponseEntity.ok(book);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/title/{titleContent}")
    public List<Book> findByTitle(@PathVariable(name = "titleContent") String title) {
        return this.bookRepository.findByTitle(title);
    }

    @PostMapping("")
    public Book addBook(@RequestBody Book book) {
        return this.bookRepository.save(book);
    }

    @PutMapping("/{bookId}")
    public ResponseEntity<Book> updateBook(@PathVariable Long bookId, @RequestBody Book bookData) {
        var optionalBook = this.bookRepository.findById(bookId);
        if (optionalBook.isPresent()) {
            var existingBook = optionalBook.get();
            if (bookData.getTitle() != null && !bookData.getTitle().isEmpty()) {
                existingBook.setTitle(bookData.getTitle());
            }
           if (bookData.getDescription() != null && !bookData.getDescription().isEmpty()) {
                existingBook.setDescription(bookData.getDescription());
           }
           if (bookData.isAvailable() != null) {
            existingBook.setAvailable(bookData.isAvailable());
           }
           return ResponseEntity.ok(this.bookRepository.save(existingBook));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{bookId}")
    public ResponseEntity<Boolean> deleteBookById(@PathVariable Long bookId) {
        var optionalBook = this.bookRepository.findById(bookId);
        if (optionalBook.isPresent()) {
            this.bookRepository.deleteById(bookId);
            return ResponseEntity.ok(true);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        }
    }

