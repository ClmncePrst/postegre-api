package com.example.postegreapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostegreApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostegreApiApplication.class, args);
	}

}
